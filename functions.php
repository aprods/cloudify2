<?php
/**
 * cloudify functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cloudify
 */
define( 'SITE_URL', esc_url( home_url( '/' ) ) );
define( 'THEME_URI', get_template_directory_uri() );
if ( ! function_exists( 'cloudify_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cloudify_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cloudify, use a find and replace
		 * to change 'cloudify' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cloudify', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __('Primary Menu'),
			'footer' => __('Cloudify Footer'),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cloudify_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'cloudify_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cloudify_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'cloudify_content_width', 640 );
}
add_action( 'after_setup_theme', 'cloudify_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cloudify_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cloudify' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cloudify' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cloudify_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function cloudify_asset_url( $path = '' ) {
  return esc_url( THEME_URI .'/'.  $path );
}
function cloudify_asset_path( $path = '' ) {
  return get_template_directory() .'/'. $path;
}
function cloudify_scripts() {

	// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( cloudify_asset_path( 'style.css' ) );

		$js_version = $theme_version . '.' . filemtime( cloudify_asset_path( '/js/main.min.js' ) );

	if ( is_front_page() ){
		//wp_enqueue_style( 'cloudify-homestyle', THEME_URI . '/style-home.css', array(), $css_version );
		wp_enqueue_style('webflow', THEME_URI . '/css/webflow.css');
		wp_enqueue_style('normalize', THEME_URI . '/css/normalize.css');
		wp_enqueue_style('cloudify.webflow', THEME_URI . '/css/cloudify.webflow.css');
		wp_enqueue_script('customjs', get_template_directory_uri() . '/js/scripts.min.js', '', 1, true);
	}else{
		wp_enqueue_style( 'cloudify-style', THEME_URI . '/style.css', array(), $css_version );

	// wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '20181215' );

	wp_enqueue_script( 'cloudify-navigation', THEME_URI . '/js/navigation.js', array(), $js_version, true );

	wp_enqueue_script( 'cloudify-skip-link-focus-fix', THEME_URI . '/js/skip-link-focus-fix.js', array(), $js_version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', "//code.jquery.com/jquery-3.3.1.min.js", array(), '20151215', false);
		wp_enqueue_script( 'jquery' );
	}
	
	wp_enqueue_script( 'main-js', THEME_URI . '/js/main.min.js', [ 'jquery' ], $js_version, true );
	}


	
	// wp_enqueue_script( 'slick', "//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js", [ 'jquery' ], '20151215', true);
	// wp_enqueue_script( 'lightbox', THEME_URI . '/js/lightbox.min.js', [ 'jquery' ], '20151215', true);	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}








	/* Make site url available to JS scripts */
	$site_parameters = array(
	    'site_url' => SITE_URL,
	    'theme_directory' => THEME_URI
	    );
	wp_localize_script( 'main-js', 'SiteParameters', $site_parameters );
}
add_action( 'wp_enqueue_scripts', 'cloudify_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';
/**
 * custom 404.
 */
require get_template_directory() . '/inc/custom404.php';

//CPT
//require get_template_directory() . '/inc/team_cpt.php';

function add_menu_attributes( $atts, $item, $args ) {


	if ( '#' === $atts['href'] ) {
        $atts['data-anchor'] = $atts['title'];
    }
	$atts['itemprop'] = 'url';

	return $atts;
}
// add_filter( 'nav_menu_link_attributes', 'add_menu_attributes', 10, 3 );

// Schema.org JSON for breadcrumbs

add_action( 'wp_footer', 'clicknathan_schema_breadcrumbs' );
function clicknathan_schema_breadcrumbs() {


	$post_id              = get_option( 'page_for_posts' );
	$post                 = get_post( $post_id );
	$slug                 = $post->post_name;
	$blog_posts_page_slug = '/' . $slug;
	$site_name            = get_bloginfo( 'blogname' );


	if ( ! is_search() ) { ?>
      <script type="application/ld+json">
	{
	 "@context": "http://schema.org",
	 "@type": "BreadcrumbList",
	 "itemListElement":
	 [<?php if ( is_singular( 'post' ) ) { // if on a single blog post ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_site_url( 'url' ) . $blog_posts_page_slug; ?>",
	    "name": "<?php echo $site_name; ?>"
	    }
	  },
	  {
	  "@type": "ListItem",
	  "position": 2,
	  "item":
	   {
	     "@id": "<?php echo get_permalink(); ?>",
	     "name": "<?php echo get_the_title(); ?>"
	   }
	  }
	 <?php } elseif ( is_singular( 'product' ) ) { // if on a single product page
			  global $post;
			  $terms = wp_get_object_terms( $post->ID, 'product_cat' );
			  if ( ! is_wp_error( $terms ) ) {
				  $product_category_slug = $terms[0]->slug;
				  $product_category_name = $terms[0]->name;
			  }
			  ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_bloginfo( 'url' ); ?>/products/<?php echo $product_category_slug; ?>/",
	    "name": "<?php echo $product_category_name; ?>"
	    }
	  },
	  {
	  "@type": "ListItem",
	  "position": 2,
	  "item":
	   {
	     "@id": "<?php echo get_permalink(); ?>",
	     "name": "<?php echo get_the_title(); ?>"
	   }
	  }
	 <?php } elseif ( is_page() && ! is_front_page() ) { // if on a regular WP Page
			  global $post;
			  if ( is_page() && $post->post_parent ) { // if is a child page
				  $post_data         = get_post( $post->post_parent );
				  $parent_page_slug  = $post_data->post_name;
				  $parent_page_url   = get_bloginfo( 'url' ) . '/' . $parent_page_slug . '/';
				  $parent_page_title = ucfirst( $parent_page_slug );
				  $position_number   = '2';
			  } else {
				  $page_url        = get_permalink();
				  $page_title      = '';
				  $position_number = '1';
			  } ?>
			  <?php if ( is_page() && $post->post_parent ) { ?>{
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo $parent_page_url; ?>",
	    "name": "<?php echo $parent_page_title; ?>"
	    }
	  },<?php } ?>
	  {
	   "@type": "ListItem",
	  "position": <?php echo $position_number; ?>,
	  "item":
	   {
	     "@id": "<?php echo get_permalink(); ?>",
	     "name": "<?php echo get_the_title(); ?>"
	   }
	  }
	 <?php } elseif ( is_home() ) { // if on the blog page ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_site_url( 'url' ) . $blog_posts_page_slug; ?>",
	    "name": "<?php echo $site_name; ?>"
	    }
	  }
	 <?php } elseif ( is_category() || is_tag() ) { ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_site_url( 'url' ) . $blog_posts_page_slug; ?>",
	    "name": "<?php echo $site_name; ?>"
	    }
	  },
	  {
	   "@type": "ListItem",
	  "position": 2,
	  "item":
	   {
	     "@id": "<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>",
	     "name": "<?php if ( is_category() ) {
			  echo single_cat_title( '', false );
		  } else {
			  echo single_tag_title( '', false );
		  } ?>"
	   }
	  }
	 <?php } elseif ( is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) { // product category and taxonomy pages
			  global $post;
			  $termname = get_query_var( 'term' );
			  $termname = ucfirst( $termname ); ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_bloginfo( 'url' ); ?>",
	    "name": "Store"
	    }
	  },
	  {
	   "@type": "ListItem",
	  "position": 2,
	  "item":
	   {
	     "@id": "<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>",
	     "name": "<?php echo $termname; ?>"
	   }
	  }
	 <?php } elseif ( is_archive() ) { // date based archives and a catch all for the rest ?>
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
	    "@id": "<?php echo get_site_url( 'url' ) . $blog_posts_page_slug; ?>",
	    "name": "<?php echo $site_name; ?>"
	    }
	  },
	  {
	   "@type": "ListItem",
	  "position": 2,
	  "item":
	   {
	     "@id": "<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>",
	     "name": "Archives"
	   }
	  }
	 <?php } else { ?>
	  {
	   "@type": "ListItem",
	  "position": 1,
	  "item":
	   {
	     "@id": "<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>",
	     "name": "Page"
	   }
	  }
	 <?php } ?>]
	}
      </script>
	<?php }
}

add_filter( 'wp_get_attachment_image_attributes', 'ipwp_img_attr', 10, 2 );
function ipwp_img_attr( $attr ) {

	$attr['itemprop'] = 'image';

	return $attr;
}
add_filter( 'show_admin_bar', '__return_false' );

// function cloudify_set_content_type(){
//     return "text/html";
// }
// add_filter( 'wp_mail_content_type','cloudify_set_content_type' );




//$bg_image_pb = $fields['perks_benefits']['bg_image'];
// if( !empty($bg_image_pb) ): 
// 	echo svg_checker($bg_image_pb,'bg_image_pb');
// 	endif; 
function svg_checker($image,$class="tc-img",$container=true){
	$img = '';
	if ($container) {
		$img .= '<div class="'.$class.'">';
	}
	
	if ($image['mime_type'] == 'image/svg+xml') {
		$img .= file_get_contents($image['url']);
	}else{
		$img .= '<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';
	}
	if ($container) {
		$img .= '</div>';
	}

	return $img;


}
// add_filter('upload_mimes', 'my_myme_types', 1, 1);
// function my_myme_types($mime_types){
// 	$mime_types['json'] = 'application/json';
// 	return $mime_types;
// }



if (!is_admin()) {
    add_filter('acf/load_value/type=textarea', 'do_shortcode');
}

$is_new_install_hrf = get_option('hrf_installed_status');
if ($is_new_install_hrf == 'new' ) {
	update_option('hrf_installed_status','news');
}



