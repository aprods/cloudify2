<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cloudify2
 */

get_header();
?>
single
<div id="primary" class="content-area">

		<?php
		while ( have_posts() ) :
			the_post();

			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix no-sidebar'); ?>>
					
						<header class="entry-header">
						<div class="header-placeholder">
							<div class="float-left">
								<?php echo do_shortcode('[share]');?>
							</div>
							<?php
								if ( is_singular() ) :
									the_title( '<h1 class="entry-title">', '</h1>' );
								else :
									the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
								endif; 
							?>
						</div>
						</header><!-- .entry-header -->
						<div class="row">
							<div class="col-sm-12">

								<div class="entry-content">

									<?php
									

									if ( 'post' === get_post_type() ) :
										?>
										<div class="entry-meta">
											<?php
											// cloudify2_posted_on();
											// cloudify2_posted_by();
											?>
										</div><!-- .entry-meta -->
									<?php endif; ?>
									<?php //cloudify2_post_thumbnail(); ?>
									<?php

									the_content( sprintf(
										wp_kses(
											/* translators: %s: Name of current post. Only visible to screen readers */
											__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cloudify2' ),
											array(
												'span' => array(
													'class' => array(),
												),
											)
										),
										get_the_title()
									) );

									// wp_link_pages( array(
									// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cloudify2' ),
									// 	'after'  => '</div>',
									// ) );
									?>
								</div><!-- .entry-content -->
									<?php echo do_shortcode('[share]');?>
								<footer class="entry-footer">
									<?php cloudify2_entry_footer(); ?>
								</footer><!-- .entry-footer -->
							<?php

							// the_post_navigation();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>
						</div>
							<!-- <div class="col-sm-4"><?php //get_sidebar(); ?></div> -->
						</div>
					
			
			</article><!-- #post-<?php //the_ID(); ?> -->
		<?php

		endwhile; // End of the loop.
		?>
</div><!-- #primary -->
			
<?php

get_footer();
