<?php
/**
 * The template for displaying all pages
 *
 * Template Name: home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cloudify2
 */

get_header();
$fields = get_fields();
// echo "<pre>";
// print_r($fields);
// echo "</pre>";
?>

<div id="primary" class="content-area">
	<main id="main" class="">




		<?php
		while ( have_posts() ) :
			the_post();

			?>
			<header class="entry-header">
				<?php
				$big_image = $fields['header_section']['big_svg'];
				if( !empty($big_image) ): 
					echo svg_checker($big_image,'big_image');
				endif; 
				echo '<p>'.$fields['header_section']['slogan'].'</p>';
				echo '<button class="btn large-btn transparent-btn">'.$fields['header_section']['cta_label'].'</button>';
				?>
			</header><!-- .entry-header -->
			<section id="what" >
				<div class="row">
					<div class="col-sm-12">
						<h1 class="js-el" id="what-title"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 125 80.49" style="enable-background:new 0 0 125 80.49;" xml:space="preserve">
							<path fill="currentColor" d="M121,36.25H17.73L63.06,7.38c1.87-1.19,2.42-3.66,1.23-5.52C63.53,0.66,62.23,0,60.91,0
							c-0.73,0-1.48,0.2-2.15,0.62L1.85,36.88C0.7,37.61,0,38.88,0,40.25s0.7,2.64,1.85,3.38l56.91,36.25c1.85,1.17,4.34,0.63,5.53-1.23
							c1.19-1.86,0.63-4.34-1.23-5.52L17.73,44.25H121c2.21,0,4-1.79,4-4S123.21,36.25,121,36.25z"/>
						</svg><?php echo $fields['what']['title'] ;?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<div class="special-p">
							<?php echo $fields['what']['right_paragraph'] ;?>
						</div>
						
					</div>
					<div class="col-sm-5 col-sm-offset-2">
						<div class="special-p">
							<?php echo $fields['what']['left_paragraph'] ;?>
						</div>
					</div>
				</div>
				<div class="yellow-section js-el" id="yellow-section">
					<div class="item_image" data-anim="cloudify2_drop" data-loop="true"><div id="cloudify2_drop"></div></div>
					
					

				<div class="row">
					<div class="col-sm-12">
						<h2 class="no-border"><?php echo $fields['yellow_section']['title'] ;?></h2>
						<div><?php echo $fields['yellow_section']['content'] ;?></div>
					</div>
				</div>
			</div>
			<div class="team-section ">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="js-el" id="team-section"><?php echo $fields['about']['team_title'] ;?></h2>
					</div>
				</div>
				<div class="row">
					<?php
					$args = array(
						'numberposts' => -1,
						'post_type'   => 'team'
					);

					$teams = get_posts( $args );

					foreach ($teams as $m) {
						$f = get_fields($m->ID);
						?>
						<div class="col-sm-2 ">
							<div class="team-member js-el" id="<?php echo preg_replace('/\s+/', '-', $m->post_title) ;?>">
								<?php echo get_the_post_thumbnail($m->ID,'thumbnail',array( 'class' => 'media-circle team-image' ) ) ;?>
								<h3><?php echo $m->post_title ;?></h3>
								<p><?php echo $f['title'] ;?><br><?php echo $f['position'] ;?></p>
							</div>
						</div>
						<?php
					}
					?>

				</div>
			</div>
			<div class="how-section">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="no-border js-el" id="how-section">
							<div class="item_image" data-anim="The_sun" data-loop="true"><div id="The_sun"></div></div>
							<?php echo $fields['about']['how_title'] ;?></h2>
					</div>
					<div class="col-sm-5">
						<p class="special-p"><?php echo $fields['about']['right_paragrpah'] ;?></p>
					</div>
					<div class="col-sm-5 col-sm-offset-2">
						<p class="special-p"><?php echo $fields['about']['left_paragraph'] ;?></p>
					</div>
				</div>
			</div>


		</section>

		<?php

	endwhile;

	?>

	<section id="why" >
		<div class="row">
			<div class="col-sm-12">
				<h1 class="js-el" id="why-section"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 125 80.49" style="enable-background:new 0 0 125 80.49;" xml:space="preserve">
					<path fill="currentColor" d="M121,36.25H17.73L63.06,7.38c1.87-1.19,2.42-3.66,1.23-5.52C63.53,0.66,62.23,0,60.91,0
					c-0.73,0-1.48,0.2-2.15,0.62L1.85,36.88C0.7,37.61,0,38.88,0,40.25s0.7,2.64,1.85,3.38l56.91,36.25c1.85,1.17,4.34,0.63,5.53-1.23
					c1.19-1.86,0.63-4.34-1.23-5.52L17.73,44.25H121c2.21,0,4-1.79,4-4S123.21,36.25,121,36.25z"/>
				</svg><?php echo $fields['why_section']['title'] ;?></h1>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php

			$i=1;
			foreach ($fields['why_section']['features'] as $feature) {

				$json = array(
					'',
					'Light',
					'money_up',
					'money',
					'V',
					'wallet',
				 );

			// echo "<pre>";
			// print_r($feature);
			// echo "</pre>";

				$class="";
			if ($i != 1){
				$class="col-sm-offset-1";
			}
			?>
			<div class="col-sm-3 <?php echo $class ;?> js-el" id="<?php echo preg_replace('/\s+/', '-', $feature['title']) ;?>">
				

					<div class="feature-item">
						<?php 
	
						// if (empty($feature['lottie']) || $feature['lottie'][0] != 'lottie') {
						// 	$feature_image = $feature['image'];
						// 	if( !empty($feature_image) ): 
						// 		echo svg_checker($feature_image,'feature_image');
						// 	endif;  
						// }else{
							echo '<div class="item_image" data-anim="'.$json[$i].'"><div id="'.$json[$i].'"></div></div>';
							?>
							
							<?php
						// }
						
						?>
						
						<h3><?php echo $feature['title'] ;?></h3>
						<div><?php echo $feature['content'] ;?></div>
					</div>
				</div>
				<?php
				$i++;
			}
			?>
		</div>
		<div class="pay-section">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="js-el" id="pay-section"><?php echo $fields['pay']['title'] ;?></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="item_image" data-anim='dots'><div id="dots"></div></div>
							
					

				<h3><?php echo $fields['pay']['right_paragraph_title'] ;?></h3>
				<div><?php echo $fields['pay']['right_paragraph'] ;?></div>
			</div>
			<div class="col-sm-6">
				<div class="item_image" data-anim='pyramid'><div id="pyramid"></div></div>
							
				
			<h3><?php echo $fields['pay']['left_paragraph_title'] ;?></h3>
			<div><?php echo $fields['pay']['left_paragraph'] ;?></div>
		</div>
	</div>
</div>
<div class="green-section js-el" id="green-section">
	<div class="row">
		<div class="col-sm-12">
			<div><?php echo $fields['green_section'] ;?></div>
		</div>
	</div>
	<svg version="1.1" id="green-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	viewBox="0 0 125 80.49" style="enable-background:new 0 0 125 80.49;" xml:space="preserve">
	<path fill="currentColor" d="M121,36.25H17.73L63.06,7.38c1.87-1.19,2.42-3.66,1.23-5.52C63.53,0.66,62.23,0,60.91,0
	c-0.73,0-1.48,0.2-2.15,0.62L1.85,36.88C0.7,37.61,0,38.88,0,40.25s0.7,2.64,1.85,3.38l56.91,36.25c1.85,1.17,4.34,0.63,5.53-1.23
	c1.19-1.86,0.63-4.34-1.23-5.52L17.73,44.25H121c2.21,0,4-1.79,4-4S123.21,36.25,121,36.25z"/>
</svg>
</div>
<div class="video-section js-el" id="video-section">
	<iframe src="https://www.youtube.com/embed/<?php echo $fields['video'] ;?>?rel=0" width="640" height="360" frameborder="0" allowfullscreen></iframe>
</div>
</section>
<section id="safe" class="js-el">
	<div class="row">
		<div class="col-sm-12">
			<h1><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 125 80.49" style="enable-background:new 0 0 125 80.49;" xml:space="preserve">
				<path fill="currentColor" d="M121,36.25H17.73L63.06,7.38c1.87-1.19,2.42-3.66,1.23-5.52C63.53,0.66,62.23,0,60.91,0
				c-0.73,0-1.48,0.2-2.15,0.62L1.85,36.88C0.7,37.61,0,38.88,0,40.25s0.7,2.64,1.85,3.38l56.91,36.25c1.85,1.17,4.34,0.63,5.53-1.23
				c1.19-1.86,0.63-4.34-1.23-5.52L17.73,44.25H121c2.21,0,4-1.79,4-4S123.21,36.25,121,36.25z"/>
			</svg><?php echo $fields['safe']['title'] ;?></h1>
		</div>
	</div>
	<div class="row justify-content-center">
		<?php

		$c = '1';
		foreach ($fields['safe']['list'] as $item) {

			$json2 = array(
					'',
					'search',
					'Flag',
					'tree',
					'umbrella',
					'drop',
				 );

			$class="";
			if ($c != 1){
				$class="col-sm-offset-1";
			}
			?>
			<div class="col-sm-3 <?php echo $class ;?> js-el" id="<?php echo preg_replace('/\s+/', '-', $json2[$c]);  ?>-section">
				<div class="list-item">
					<?php 
						
						// if (empty($item['lottie']) || $item['lottie'][0] != 'lottie') {
						// 	$item_image = $item['image'];
						// 	if( !empty($item_image) ): 
						// 		echo svg_checker($item_image,'item_image');
						// 	endif;  
						// }else{
							echo '<div class="item_image" data-anim="'.$json2[$c].'"><div id="'.$json2[$c].'"></div></div>';
							?>

							<?php
						// }
						
						?>




					<span><?php echo $c.'.';?></span>

					<div><?php echo $item['content'] ;?></div>
				</div>
			</div>
			<?php
			$c++;
		}
		?>
	</div>
	<div class="blue-section js-el" id="blue-section">
		<div class="row">
			<div class="col-sm-12">
				<?php echo $fields['blue_section'] ;?>
			</div>
		</div>
	</div>
</section>
<section id="faq" >
	<div class="row">
		<div class="col-sm-12">
			<h1 class="js-el" id="faq-section"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 125 80.49" style="enable-background:new 0 0 125 80.49;" xml:space="preserve">
				<path fill="currentColor" d="M121,36.25H17.73L63.06,7.38c1.87-1.19,2.42-3.66,1.23-5.52C63.53,0.66,62.23,0,60.91,0
				c-0.73,0-1.48,0.2-2.15,0.62L1.85,36.88C0.7,37.61,0,38.88,0,40.25s0.7,2.64,1.85,3.38l56.91,36.25c1.85,1.17,4.34,0.63,5.53-1.23
				c1.19-1.86,0.63-4.34-1.23-5.52L17.73,44.25H121c2.21,0,4-1.79,4-4S123.21,36.25,121,36.25z"/>
			</svg><?php echo $fields['faq_title'] ;?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<?php echo do_shortcode('[hrf_faqs]'); ?>
		</div>
	</div>
</section>

		<!-- <section id="registration">
			<div class="row">
				<div class="col-sm-12">
					<article>
						registration
					</article>
				</div>
			</div>
		</section> -->



	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
