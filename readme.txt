
Theme Name: cloudify2
Theme URI: https://gitlab.com/aprods/cloudify2#readme
Author: Buzzhunter
Author URI: https://buzzhunter.co
Description: 
Version: v1.0.1
License: GPL-2.0-or-later
License URI: (http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html)
 * cloudify2 is based on _s (https://underscores.me/),Automattic (https://automattic.com/),Nicolas Gallagher and Jonathan Neal (https://necolas.github.io/normalize.css/)
 * Copyright 2018-2019 Buzzhunter
