<?php get_header('wb');?>

<div class="container-home">
	<?php if (have_posts()) : while(have_posts()) : the_post();?>

		<?php the_content();?>

	<?php endwhile; endif;?>
</div>



<?php get_footer('wb');?>
