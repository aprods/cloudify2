<?php
/**
 * cloudify2 Theme Customizer
 *
 * @package cloudify2
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function cloudify2_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// $allPosts = get_posts( array(
	// 				'numberposts' => -1,
	// 			    'post_type' => array('page','post'),
	// 			    'orderby'   => 'post__in',
	// 			));
	// foreach ( $allPosts as $post ) {
	// 	$posts[$post->ID] = $post->post_title;
        
 //    }

    // Add page dropdown

	// $wp_customize->add_setting( 'tc' , array( 'default' => '' ));
	// $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tc', array(
	//     'label' => __( '"T&C " page', 'cloudify2' ),
	//     'section' => 'title_tagline',
	//     'settings' => 'tc',
	//     'type'    => 'select',
 //    		'choices' => $posts
	// ) ) );

	// $wp_customize->add_setting( 'tt' , array( 'default' => '' ));
	// $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tt', array(
	//     'label' => __( '"terms (trumot) " page', 'cloudify2' ),
	//     'section' => 'title_tagline',
	//     'settings' => 'tt',
	//     'type'    => 'select',
 //    		'choices' => $posts
	// ) ) );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'cloudify2_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'cloudify2_customize_partial_blogdescription',
		) );
	}
	// Add Copyright 
	$wp_customize->add_setting( 'copyright' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'copyright', array(
	    'label' => __( 'copyright', 'cloudify2' ),
	    'section' => 'title_tagline',
	    'settings' => 'copyright',
	    'priority' => 30,
	) ) );
	// Add Social Media Section
	$wp_customize->add_section( 'social-media' , array(
	    'title' => __( 'Social Media', 'cloudify2' ),
	    'priority' => 30,
	    'description' => __( 'Enter the URL to your account for each service for the icon to appear in the footer.', 'cloudify2' )
	) );
	// Add Youtube Setting
	$wp_customize->add_setting( 'youtube' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube', array(
	    'label' => __( 'Youtube', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'youtube',
	) ) );
	// Add Twitter Setting
	$wp_customize->add_setting( 'twitter' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
	    'label' => __( 'Twitter', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'twitter',
	) ) );
	// Add Facebook Setting
	$wp_customize->add_setting( 'facebook' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
	    'label' => __( 'Facebook', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'facebook',
	) ) );
	// Add Linkedin Setting
	$wp_customize->add_setting( 'linkedin' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'linkedin', array(
	    'label' => __( 'Linkedin', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'linkedin',
	) ) );
	// Add instagram Setting
	$wp_customize->add_setting( 'instagram' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array(
	    'label' => __( 'Instagram', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'instagram',
	) ) );
	// Add telegram Setting
	$wp_customize->add_setting( 'telegram' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'telegram', array(
	    'label' => __( 'Telegram', 'cloudify2' ),
	    'section' => 'social-media',
	    'settings' => 'telegram',
	) ) );

	// Add Custom Code
	$wp_customize->add_section( 'additionalCode' , array(
	    'title' => __( 'Additional Code', 'cloudify2' ),
	    'priority' => 30,
	    'description' => __( 'Add code to the header and footer', 'cloudify2' )
	) );
	$wp_customize->add_setting( 'headerCode' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'headerCode', array(
	    'label' => __( '"Header Code', 'cloudify2' ),
	    'section' => 'additionalCode',
	    'settings' => 'headerCode',
	    'type'    => 'textarea',
	) ) );
	$wp_customize->add_setting( 'footerCode' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footerCode', array(
	    'label' => __( '"Footer Code', 'cloudify2' ),
	    'section' => 'additionalCode',
	    'settings' => 'footerCode',
	    'type'    => 'textarea',
	) ) );
	$wp_customize->add_setting( 'apiKey' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'comeet_groups', array(
	    'label' => __( 'Google Map API Key', 'cloudify2' ),
	    'section' => 'additionalCode',
	    'settings' => 'apiKey',
	) ) );
}
add_action( 'customize_register', 'cloudify2_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function cloudify2_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function cloudify2_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function cloudify2_customize_preview_js() {
	wp_enqueue_script( 'cloudify2-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'cloudify2_customize_preview_js' );
