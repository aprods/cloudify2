<?php
// Social shortcode
function social_func( $atts ) {
	$linkedin ='';
	$twitter ='';
	$facebook ='';
	$instagram ='';
	$youtube ='';
	$telegram ='';

	if ( get_theme_mod('linkedin') ) {
		$linkedin = '<a class="linkedin-icon" href="'.get_theme_mod('linkedin') .'" target="_blank"><i class="fab fa-linkedin-in"></i></a>';
	}
	if ( get_theme_mod('twitter') ) {
		$twitter = '<a class="twitter-icon" href="'.get_theme_mod('twitter') .'" target="_blank"><i class="fab fa-twitter"></i></a>';
	}
	if ( get_theme_mod('facebook') ){
		$facebook = '<a class="facebook-icon" href="'.  get_theme_mod('facebook') .'" target="_blank"><i class="fab fa-facebook-f"></i></a>';
	}
	if ( get_theme_mod('instagram') ){
		$instagram = '<a class="instagram-icon" href="'.  get_theme_mod('instagram') .'" target="_blank"><i class="fab fa-instagram"></i></a>';
	}
	if ( get_theme_mod('youtube') ) {
		$youtube = '<a class="youtube-icon" href="'.get_theme_mod('youtube') .'" target="_blank"><i class="fab fa-youtube"></i></a>';
	}
	if ( get_theme_mod('telegram') ) {
		$linkedin = '<a class="telegram-icon" href="'.get_theme_mod('telegram') .'" target="_blank"><i class="fab fa-telegram-plane"></i></a>';
	}

	$social='<div class="social">'. $linkedin .$twitter.  $facebook . $instagram . $youtube ;
	$social.='<script type="application/ld+json">
				{
					"@context": "http://schema.org/",
					"@type": "Organization",
						"url":"'.home_url().'",
				        "sameAs": [
				    "'.  get_theme_mod('facebook') .'",
				    "'.get_theme_mod('twitter') .'",
				    "'.  get_theme_mod('instagram') .'",
				    "'.get_theme_mod('linkedin') .'",
				    "'.get_theme_mod('youtube') .'",
				    "'.get_theme_mod('telegram') .'"
				  ]
				}
				</script>';
	$social.='</div>';
				
	return $social;
}
add_shortcode( 'social', 'social_func' );

// [desktoponly] shortcode
add_shortcode('desktoponly', 'shailan_desktop_only_shortcode');
function shailan_desktop_only_shortcode($atts, $content = null){ 
    if( !wp_is_mobile() ){ 
        return wpautop( do_shortcode( $content ) ); 
    } else {
        return null; 
    } 
}
 
// [mobileonly] shortcode
add_shortcode('mobileonly', 'shailan_mobile_only_shortcode');
function shailan_mobile_only_shortcode($atts, $content = null){ 
    if( wp_is_mobile() ){ 
        return  wpautop( do_shortcode( $content ) ); 
    } else {
        return null; 
    } 
}

add_shortcode('share', 'shortcode_share');
function shortcode_share( $atts){
	// echo "<pre>";
	// print_r($atts);
	// echo "</pre>";
      // SOCIAL SHARING
      // Get current page URL
      $sharedURL = get_permalink();
      // $summary = get_the_excerpt();
      $summary = str_replace( ' ', '%20', get_the_excerpt());
      // Get current page title
      $sharedTitle = str_replace( ' ', '%20', get_the_title());
      // Get Post Thumbnail for pinterest
      $sharedThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
      // Construct sharing URL without using any script
      $twitterURL = 'https://twitter.com/intent/tweet?text='.$sharedTitle.' - '.$summary.'&amp;url='.$sharedURL;
      $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$sharedURL;
      $linkedInURL ='https://www.linkedin.com/shareArticle?mini=true&url='.$sharedURL.'&title='.$sharedTitle.'&summary='.$summary.'&source='.site_url();
      $whatssappURL ='https://api.whatsapp.com/send?text='.$sharedTitle.'%20at:%20'.$sharedURL;

      $share = '<div class="share-post">
      <div class="shared-social">
       <span>'. __( "Share:", "cloudify2").'</span>
		 <div class="share-social-links">  
		 	<a class="shared-link shared-facebook" data-network="facebook" href="'.$facebookURL.'" target="_blank"><i class="fab fa-facebook-f"></i></a>
			<a class="shared-link shared-twitter" data-network="twitter" href="'.$twitterURL.'" target="_blank"><i class="fab fa-twitter"></i></a>
			<a class="shared-link shared-linkedIn" data-network="linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fab fa-linkedin-in"></i></a>
			<a class="shared-link shared-whatsapp" data-network="whatsapp" href="'.$whatssappURL.'" target="_blank"><i class="fab fa-whatsapp"></i></a>
        </div>
      </div>
  </div>';
  return $share;
}

// [tooltip] shortcode
add_shortcode('tooltip', 'cloudify2_tooltip_shortcode');
function cloudify2_tooltip_shortcode($atts, $content = null){ 

    $tooltip ='<span class="tooltip"><span class="tp_trigger">!</span>';
	    $tooltip .='<span class="top">';
	        $tooltip .='<span>'.$atts['content'].'</span>';
	        $tooltip .='<i></i>';
	    $tooltip .='</span>';
	$tooltip .='</span>';




    return $tooltip; 
}






