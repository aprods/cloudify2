<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Tue Jan 29 2019 12:11:17 GMT+0000 (UTC)  -->
<head>
	<meta charset="utf-8">
	<title><?php single_post_title(); ?></title>
	<meta content="<?php single_post_title(); ?>" property="og:title">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<meta content="Webflow" name="generator">
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
	<script type="text/javascript">WebFont.load({  google: {    families: ["Quicksand:300,regular,500,700:latin-ext,latin","Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic"]  }});</script>
	<!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
	<script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
	<!--<link href="//daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">-->
	<link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
	<script type="text/javascript">WebFont.load({  google: {    families: ["Quicksand:300,regular,500,700:latin-ext,latin","Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic"]  }});</script>
	<!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
	<script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
	<?php wp_head();?>

	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-31930048-1', 'auto');
		ga('require', 'GTM-PV6R5BV');
		//  ga('require', 'eventTracker');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P4L26C');</script>
	<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
</head>
<body>
<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar-2 w-nav">
	<div class="container-8 w-container"><a href="/" class="brand-2 w-nav-brand"><img src="/wp-content/themes/cloudify2/images/Cloudify-Logo-2019_2-01.svg" width="200" alt=""></a>
		<nav role="navigation" class="nav-menu-2 w-nav-menu">
			<nav class="nav-menu-2 w-nav-menu">
				<?php

				$args = array(
					'menu' => 'cloudify',
					'theme_location' => 'primary',
				);

				?>
				<?php wp_nav_menu($args);?>
			</nav>

		</nav>
		<div class="hd-search">
			<form method="get" class="search-form" action="<?php echo esc_url( wp_make_link_relative( home_url( '/' ) ) ); ?>">

				<input type="search" class="form-control" placeholder="<?php esc_attr_e( 'Search...', 'consulting' ); ?>" value="" name="s" />
				<button type="submit"><i class="fa fa-search"></i></button>

			</form>
		</div>
		<div class="w-nav-button">
			<div class="w-icon-nav-menu"></div>
		</div>
	</div>
</div>