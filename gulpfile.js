// Load dependencies.
var gulp = require('gulp'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    header = require('gulp-header');
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    pkg = require('./package.json'),
    browserSync = require('browser-sync').create();

// Set the banner content
var banner = ['/*!\n',

        'Theme Name: <%= pkg.title %>\n',
        'Theme URI: <%= pkg.homepage %>\n',
        'Author: <%= pkg.author %>\n',
        'Author URI: <%= pkg.authorURL%>\n',

        'Description: <%= pkg.description %>\n',
        'Version: v<%= pkg.version %>\n',
        'License: <%= pkg.license %>\n',
        'License URI: (<%= pkg.licenseURL %>)\n',

        ' * <%= pkg.title %> is based on <%= pkg.contributors %>\n',
        ' * Copyright 2018-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
        '\n',
        '*/\n',

    '\n'
].join('');

// Aggregate and minify sass files.
gulp.task('sass', function () {
    return gulp.src('./sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(gulp.dest('./'));
});

// Minify the main JS file.
gulp.task('scripts', function () {
    return gulp.src('./js/main.js')
        .pipe(uglify())
        .on('error', function (err) {console.log(err.toString());})
        .pipe(rename({suffix: ".min"}))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulp.dest('./js'));
});

// Watch for changes in source files.
gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass',browserSync.reload]);
    gulp.watch('./js/main.js', ['scripts',browserSync.reload]);
    gulp.watch('./*.php', browserSync.reload);
});



// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    proxy: "http://localhost/cloudify2"
  });
});

// Dev task
gulp.task('default', ['scripts',  'sass', 'watch', 'browserSync']);

// Prod task to run.
gulp.task('prod', ['scripts',  'sass']);


