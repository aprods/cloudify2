<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cloudify2
 */

$fields = get_fields();
// echo "<pre>";
// print_r($fields);
// echo "</pre>";
?>
<div class="modal">
	<div class="modal_inner">
	<div class="modal_content">
		<div class="close">&times;</div>
		<div class="steps">
			<div class="step-0">1</div>
			<div class="step-1">2</div>
			<div class="step-2">3</div>
			<p>לאסימון הרשמה</p>
		</div>

		<?php 
		 $frontpage_id = get_option('page_on_front');
		 echo do_shortcode('[contact-form-7 id="'.get_field('registration_form_id', $frontpage_id).'" title="registration"]');
		 ;
		?>
		<div class="successMessage successModal">
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
			  <circle class="path circle" fill="none" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
			  <polyline class="path check" fill="none" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
			</svg>
			<?php echo get_field('success_message',$frontpage_id)  ?>
		</div>
	</div>
	</div>
</div>
<div class="successMessage successNotModal">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
  <circle class="path circle" fill="none" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
  <polyline class="path check" fill="none" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
</svg>
<?php echo get_field('success_message')  ?>
</div>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<?php
					wp_nav_menu( array(
						'theme_location' => 'footer-menu',
						'menu_id'        => 'footer',
					) );
					?>

			<div class="footer-row">
			<div class="copy">
					<?php echo get_theme_mod('copyright');?>
			</div>
			
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
	
</div>
<?php echo get_theme_mod('footerCode'); ?>
<?php wp_footer(); ?>

</body>
</html>
