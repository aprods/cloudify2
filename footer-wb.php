<!--<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js?v1" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->

<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<?php wp_footer();?>
	<div class="section _60 footer">
		<div class="div-block-6"><img src="/wp-content/themes/cloudify2/images/Cloudify-Logo-WHITE-2019_2-03.svg" width="150" alt="" class="image-6">
			<div class="text-block-3">Cloudify provides universal edge orchestration that enables service providers and enterprises to automate, manage and virtually transform their network and application services from their core location to branches and multi-access edge devices.</div>
			<div class="div-block-28">
				<a href="https://twitter.com/CloudifySource">
					<img src="/wp-content/themes/cloudify2/images/Twitter.png" alt="" class="footersocial">
				</a>
				<a href="https://www.linkedin.com/company/cloudifyco/">
					<img src="/wp-content/themes/cloudify2/images/LinkedIn.png" alt="" class="footersocial">
				</a>
				<a href="https://github.com/cloudify-cosmo/">
					<img src="/wp-content/themes/cloudify2/images/Github.png" alt="" class="footersocial">
				</a>
				<a href="https://soundcloud.com/theopensourcepodcast">
					<img src="/wp-content/themes/cloudify2/images/Soundcloud.png" alt="" class="footersocial">
				</a>
				<a href="http://youtube.com/user/cloudifysource">
					<img src="/wp-content/themes/cloudify2/images/Youtube.png" alt="" class="footersocial">
				</a>
			</div>
		</div>
		<div class="div-block-5">
			<?php

			$args = array(
				'theme_location' => 'footer',
			);

			?>
			<?php wp_nav_menu( $args );?>
		</div>
	</div>
</body>
</html>